import os
import time

class Logger():

    def __init__(self):

        # create Log directory if not exist
        self.directory = 'Log'
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        # write new line for better formating
        with open(os.path.join(self.directory, 'log_file.txt'), 'a') as log_file:
            msg_content = '\n'
            log_file.write(msg_content)

    def log(self, message, status):

        # write log message
        with open(os.path.join(self.directory, 'log_file.txt'), 'a') as log_file:
            msg_content = '{0}: {1} ---> {2}\n\n'.format(time.asctime(time.localtime(time.time())), status, message)
            log_file.write(msg_content)
            return msg_content
