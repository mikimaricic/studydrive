import unittest

from selenium import webdriver
from pages import home_page, dashboard_page

class TestScenario(unittest.TestCase):

    def initialize_driver(self):
        self.DRIVER = None

        self.HANDLER_HOME = None
        self.HANDLER_DASHBOARD = None

    @classmethod
    def setUpClass(cls):
        
        # initialize driver
        cls.initialize_driver(cls)

    def test_0_User_Login(self):

        # enter and confirm user login data
        self.HANDLER_HOME.login()

    def test_1_Test_Page_New_tab(self):

        # open test page in new tab
        self.HANDLER_HOME.test_page_new_tab()
        
    def test_2_Select_Course_File_Upload(self):

        # select course where to upload file
        self.HANDLER_DASHBOARD.select_course_file_upload()

    def test_3_Select_Upload_File_Description(self):

        # select upload file and leave description empty
        self.HANDLER_DASHBOARD.select_upload_file_description()

    def test_4_Select_Upload_File_Semester(self):

        # select upload file and leave description empty
        self.HANDLER_DASHBOARD.select_upload_file_semester()

    def test_5_Select_Upload_File_Type(self):

        # select upload file and leave description empty
        self.HANDLER_DASHBOARD.select_upload_file_type()

    def test_6_Select_Upload_File_Full(self):

        # select upload file and leave description empty
        self.HANDLER_DASHBOARD.select_upload_file_full()

    @classmethod
    def tearDownClass(cls):

        # driver quit and exit
        cls.DRIVER.quit()

class ChromeScenario(TestScenario):

    def initialize_driver(self):
        self.DRIVER = webdriver.Chrome()
        self.DRIVER.maximize_window()

        self.HANDLER_HOME = home_page.HomePage(self.DRIVER)
        self.HANDLER_DASHBOARD = dashboard_page.DashboardPage(self.DRIVER)
        
        # navigate to the test page
        self.DRIVER.get('https://www.studydrive.net/file-upload')

class FirefoxScenario(TestScenario):

    def initialize_driver(self):
        self.DRIVER = webdriver.Firefox()
        self.DRIVER.maximize_window()

        self.HANDLER_HOME = home_page.HomePage(self.DRIVER)
        self.HANDLER_DASHBOARD = dashboard_page.DashboardPage(self.DRIVER)
        
        # navigate to the test page
        self.DRIVER.get('https://www.studydrive.net/file-upload')