from utility import logger
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class HomePage():
    
    def __init__(self, driver):
        self.driver = driver

        # define user data
        self.test_user_name = 'mikimaricic@yahoo.com'
        self.test_user_password = 'Dubika1980'

        # define locators
        self.login_user_name_element = 'email'
        self.login_password_element = 'password'
        self.login_button_element = 'login-btn'
        self.confirm_login_data_element = 'js-login-btn'
        self.login_confirmed_element = 'upload-button'
        
        # define driver web element wait time
        self.element_wait_time = 10
        self.wait = WebDriverWait(self.driver, self.element_wait_time)

        # define logger
        self.logger_handle = logger.Logger()

    def login(self):
        try:

            # enter user name in login form
            login = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, self.login_button_element)))
            login.click()
            user_name_input = self.wait.until(EC.element_to_be_clickable((By.NAME, self.login_user_name_element)))
            user_name_input.send_keys(self.test_user_name)

            # enter user password in login form
            password_input = self.wait.until(EC.element_to_be_clickable((By.NAME, self.login_password_element)))
            password_input.send_keys(self.test_user_password)
            
            # click login button
            click_login = self.wait.until(EC.element_to_be_clickable((By.ID, self.confirm_login_data_element)))
            click_login.click()

            # log action
            self.logger_handle.log('Input login data and confirm ', 'SUCCESS')

        except Exception as ex:
            #log exception
            self.logger_handle.log('Input login data and confirm {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()


    def test_page_new_tab(self):
        try:
            
            # open test URL in new tab to show handle switch
            url_to_open = 'https://www.studydrive.net/file-upload'

            # wait login proces to be completed
            login_wait = self.wait.until(EC.presence_of_element_located((By.ID, self.login_confirmed_element)))
            self.driver.execute_script('window.open("{}");'.format(url_to_open))

            # switch to new window
            windows_after = self.driver.window_handles[1]
            self.driver.switch_to_window(windows_after)

            # log action
            self.logger_handle.log('Open test URL in new tab ', 'SUCCESS')

        except Exception as ex:
            #log exception
            self.logger_handle.log('Open test URL in new tab {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()