from utility import logger
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
from pathlib import Path
from docx import Document
import random
import time
import os
import sys

class DashboardPage():
    
    def __init__(self, driver):
        self.driver = driver

        #define data
        self.course_name = 'Quality Assurance'
        self.finish_upload_button_text = 'Finish upload'
        self.description_failed_text = 'The description is not filled!'
        self.semester_failed_text = 'The semester is not filled!'
        self.type_failed_text = 'The file type is not filled!'
        self.description_text = 'Test Description'
        self.document_name = 'test{}.docx'.format(random.sample(range(100, 1000), 1))
        self.success_uploaded_text = 'Your upload was successful'
        self.test_url = ('https://www.studydrive.net/file-upload')
        self.semester_text = 'Summer 2019'
        self.type_text = 'Other'

        # define locators
        self.choose_course_button = 'course-selector-selectized'
        self.upload_file_description_element = 'qqfile'
        self.finish_upload_button = "//*[@id='js-upload-container']//button[contains(text(),'{}')]".format(self.finish_upload_button_text)
        self.file_description_empty_faild_element = "//*[@id='file-0']//span[contains(text(),'{}')]".format(self.description_failed_text)
        self.file_semester_empty_faild_element = "//*[@id='file-0']//span[contains(text(),'{}')]".format(self.semester_failed_text)
        self.file_description_enter_element = 'description'
        self.close_dialog_element = 'delete-button'
        self.file_semester_enter_element = "//*[@id='file-0']//option[text()='{}']".format(self.semester_text)
        self.file_semester_class_element = 'semester'
        self.file_type_empty_faild_element =  "//*[@id='file-0']//span[contains(text(),'{}')]".format(self.type_failed_text)
        self.file_type_class_element = 'type'
        self.file_type_enter_element = "//*[@id='file-0']//option[text()='{}']".format(self.type_text)
        self.file_success_uploaded_element = "//*[@id='swal2-content']//p[contains(text(),'{}')]".format(self.success_uploaded_text)
        
        # define driver web element wait time
        self.element_wait_time = 10
        self.wait = WebDriverWait(self.driver, self.element_wait_time)

        # define logger
        self.logger_handle = logger.Logger()

        # create file to upload
        self.create_file()

    def select_course_file_upload(self):
        try:
            
            # select course where to upload file
            choose_course = self.wait.until(EC.element_to_be_clickable((By.ID, self.choose_course_button)))
            choose_course.send_keys(self.course_name)
            choose_course.send_keys(Keys.ENTER)

            # log action
            self.logger_handle.log('Select course for file upload ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Select course for file upload {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def create_file(self):
        try:
            
            # create file to upload
            document = Document()
            document.add_heading('Test Document', 0)
            document.add_paragraph('test text ')
            document.save(self.document_name)

            # log action
            self.logger_handle.log('Create test file for upload ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Create test file for upload {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def click_upload(self):
        try:
            
             # click on "Finish upload" button to upload selected file
            file_desc_empty_upload = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.finish_upload_button)))
            file_desc_empty_upload.click()

            # log action
            self.logger_handle.log(' # click on "Finish upload" button ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log(' # click on "Finish upload" button {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def select_file_and_upload(self):
        try:
            
            # select file for upload
            upload_file_description = self.driver.find_element_by_name(self.upload_file_description_element)
            ROOT_DIR = Path(os.path.dirname(sys.modules['__main__'].__file__)).parent
            upload_file_description.send_keys(os.path.join(ROOT_DIR, self.document_name))

            # click on "Finish upload" button to upload selected file
            self.click_upload()

            # log action
            self.logger_handle.log('Select file and upload ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Select file and upload {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def enter_description(self):
        try:

            # enter document description
            file_description_text_faild = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, self.file_description_enter_element)))
            file_description_text_faild.send_keys(self.description_text)

            # log action
            self.logger_handle.log('Enter document description ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Enter document description {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def enter_semester(self):
        try:

            # enter semester
            file_semester = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, self.file_semester_class_element)))
            file_semester.click()
            file_semester_chose = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.file_semester_enter_element)))
            file_semester_chose.click()

            # log action
            self.logger_handle.log('Enter semester period ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Enter semester period {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def select_upload_file_description(self):
        try:
            
            #select and upload file with description field empty
            self.select_file_and_upload()

            # check that "The description is not filled!" error text exist
            file_description_empty_faild = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.file_description_empty_faild_element)))

            # close file upload dialog box
            close_button = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, self.close_dialog_element)))
            close_button.click()

            # log action
            self.logger_handle.log('Select file and upload with description empty ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Select file and upload with description empty {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def select_upload_file_semester(self):
        try:

            # load upload file form
            self.driver.get(self.test_url)
            self.select_course_file_upload()
            
            #select and upload file with semester field empty
            self.select_file_and_upload()

            # enter description text
            self.enter_description()

            # click on "Finish upload" button to upload selected file
            self.click_upload()

            # check that "The semester is not filled!" error text exist
            file_semester_empty_faild = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.file_semester_empty_faild_element)))

            # log action
            self.logger_handle.log('Select file and upload with semester empty ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Select file and upload with semester empty {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def select_upload_file_type(self):
        try:

            # load upload file form
            self.driver.get(self.test_url)
            self.select_course_file_upload()
            
            #select and upload file with type field empty
            self.select_file_and_upload()

            # enter description text
            self.enter_description()

            # enter semester
            self.enter_semester()

            # click on "Finish upload" button to upload selected file
            self.click_upload()

            # check that "The file type is not filled!" error text exist
            file_type_empty_faild = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.file_type_empty_faild_element)))

            # log action
            self.logger_handle.log('Select file and upload with type empty ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Select file and upload with type empty {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()

    def select_upload_file_full(self):
        try:

            # load upload file form
            self.driver.get(self.test_url)
            self.select_course_file_upload()
            
            #select and upload file with type field empty
            self.select_file_and_upload()

            # enter description text
            self.enter_description()

            # enter semester
            self.enter_semester()

            # enter type text
            file_type = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, self.file_type_class_element)))
            file_type.click()
            file_type_chose = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.file_type_enter_element)))
            file_type_chose.click()

            # click on "Finish upload" button to upload selected file
            self.click_upload()

            # check that document is uploaded
            file_type_empty_faild = self.wait.until(EC.element_to_be_clickable((By.XPATH, self.file_success_uploaded_element)))

            # delete test document
            os.remove(self.document_name)

            # log action
            self.logger_handle.log('Select file and upload with type empty ', 'SUCCESS')

        except Exception as ex:
            # log exception
            self.logger_handle.log('Select file and upload with type empty {0}'.format(str(ex)), 'FAILED')
            raise AssertionError()