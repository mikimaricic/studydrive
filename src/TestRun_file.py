import unittest
import os

from utility import HTMLTestRunner
from TestScenario import ChromeScenario, FirefoxScenario
from threading import Thread

'''
Run Test Suite and generate HTML report
'''
def run_suite(test_class, browser):
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(test_class)
    big_suite = unittest.TestSuite(suite)

    direct = os.getcwd()
    outfile = open(os.path.join(direct, 'Test-{}.html'.format(browser)), 'w')
    runner = HTMLTestRunner.HTMLTestRunner(
        stream=outfile,
        title='Tests Scenario',
        description='Tests Scenario Report'
    )
    runner.run(big_suite)

'''
Run Test Case on Chrome browser, Use separated thread
'''
def run_chrome():
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(ChromeScenario)
    big_suite = unittest.TestSuite(suite)

    direct = os.getcwd()
    outfile = open(os.path.join(direct, 'Test-{}.html'.format("Chrome")), 'w')
    runner = HTMLTestRunner.HTMLTestRunner(
        stream=outfile,
        title='Test Report',
        description='Tests Scenario Report'
    )
    runner.run(big_suite)

'''
Run Test Case on Firefox browser, Use separated thread
'''
def run_firefox():
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(FirefoxScenario)
    big_suite = unittest.TestSuite(suite)

    direct = os.getcwd()
    outfile = open(os.path.join(direct, 'Test-{}.html'.format("Firefox")), 'w')
    runner = HTMLTestRunner.HTMLTestRunner(
        stream=outfile,
        title='Test Report',
        description='Tests Scenario Report'
    )
    runner.run(big_suite)

'''
    This is script entery point
'''
if __name__ == "__main__":
    chrome_thread = Thread(target=run_chrome)
    firefox_thread = Thread(target=run_firefox)
    
    firefox_thread.start()
    chrome_thread.start()
    
    firefox_thread.join()
    chrome_thread.join()