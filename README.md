# Studydrive automated test
Login to "Studydrive" login page, navigate to "Upload Document page" and test the following journey:

## Test Casees
  - Verified user can upload documents
  - Blacklist of document extensions
  - Limit of the number of documents
  - Maximum document size
  - Upload process is asynchronous
  - Uploaded document is attached to the course/module
  - Uploaded document has a preview (on its detail page) and can be downloaded

## Requirements
  - 'python 3.7.3' 
  - 'pip'
  - Chrome and Firefox browser
  - Web driver for Chrome and Firefox

## Steps to prepare clean machine (you can skip install already installed programs on your machine)
  - Install Chrome browser ()
  - Install Firefox browser ()
  - Install Git (https://git-scm.com/download/win)
  - Clone repo: 'https://bitbucket.org/mikimaricic/studydrive.git'
  - Install Python 3.7.3 (https://www.python.org/downloads/release/python-373/)
  - Install dependencies with 'pip install -r requirements.txt'
  - Download chrome-driver (https://github.com/mozilla/geckodriver/releases/tag/v0.26.0)
  - Download firefox-driver (https://github.com/mozilla/geckodriver/releases/tag/v0.26.0)
      Set drivers:
        * add it to your system path (recommended because script is adjusted for this option)
        * put it in the same directory as your python script

## How to execute
Run test case like this:
  - to run script you have to:
      - In Visual Studio Code run TestRun_file.py with F5 and confirm python language if needed
      - In code editor
        1. position yourself in script folder
        2. run TestRun_file.py with command line: 'python .\src\TestRun_file.py' and confirm python language if needed

## Reporting
  - All execution steps are logged in .\Log\log_file.txt
  - Full test HTML report for Chrome browser is located in script root '.\Test-Chrome.html'
  - Full test HTML report for Firefox browser is located in script root '.\Test-Firefox.html'
